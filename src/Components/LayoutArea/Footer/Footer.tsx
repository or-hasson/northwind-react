import React from 'react';
import './Footer.css';

function Footer() : JSX.Element { // JSX.Element = UI object {
    return (
        <div className="Footer">
            <p> All Rights Reserved &copy;</p>
        </div>
    );
}

export default Footer;